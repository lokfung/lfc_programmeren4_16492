<?php
    namespace Lok\Competitie\Model;
    
    class Speler
    {
        private $familienaam;
        private $voornaam;
        private $adres;
        private $rugnummer;
        
        //FAMILIENAAM
        public function SetFamilienaam($familienaam){
            //je gebruikt een setter als je wilt controleren
            //of de waarde die je wilt toekennen aan een veld
            //een geschikte waarde is
            //indien dat niet nodig is, maa je het veld public
            if (empty($familienaam)){
                return false;
            }
            else{
                $this->familienaam = $familienaam;
                return true;
            }
        }
        
        public function GetFamilienaam(){
            return $this->familienaam;
        }
        
        
        //VOORNAAM
        public function SetVoornaam($voornaam){
            //je gebruikt een setter als je wilt controleren
            //of de waarde die je wilt toekennen aan een veld
            //een geschikte waarde is
            //indien dat niet nodig is, maa je het veld public
            if (empty($voornaam)){
                return false;
            }
            else{
                $this->voornaam = $voornaam;
                return true;
            }
        }
        
        public function GetVoornaam(){
            return $this->voornaam;
        }
        
        
        //ADRES
        public function SetAdres($adres){
            //je gebruikt een setter als je wilt controleren
            //of de waarde die je wilt toekennen aan een veld
            //een geschikte waarde is
            //indien dat niet nodig is, maa je het veld public
            if (empty($adres)){
                return false;
            }
            else{
                $this->adres = $adres;
                return true;
            }
        }
        
        public function GetAdres(){
            return $this->adres;
        }
        
        
        //RUGNUMMER
        public function SetRugnummer($rugnummer){
            //je gebruikt een setter als je wilt controleren
            //of de waarde die je wilt toekennen aan een veld
            //een geschikte waarde is
            //indien dat niet nodig is, maa je het veld public
            if (empty($rugnummer)){
                return false;
            }
            else{
                $this->rugnummer = $rugnummer;
                return true;
            }
        }
        
        public function GetRugnummer(){
            return $this->rugnummer;
        }
    }