<?php
namespace Lok\Competitie\Model;

class Wedstrijd
{
    private $datum;
    private $tijd;
    private $status;
    private $scoreThuis;
    private $scoreBezoekers;
    
    //je gebruikt een setter als je wilt controleren
    //of waarde die je wilt toekennen aan een veld
    //een geschikte waarde is
    //indien dat niet nodig is, maak je het veld public
    public function setDatum($datum) 
    {
        if (empty ($datum))
        {
            return false;
        } else {
            $this->datum = $datum;
            return true;
        }
    }
    
    public function getDatum() 
    {
        return $this->datum;
    }
    
    public function setTijd($tijd) 
    {
        if (empty ($tijd))
        {
            return false;
        } else {
            $this->tijd = $tijd;
            return true;
        }
    }
    
    public function getTijd() 
    {
        return $this->tijd;
    }
    
    public function setStatus($status) 
    {
        if (empty ($status))
        {
            return false;
        } else {
            $this->status = $status;
            return true;
        }
    }
    
    public function getStatus() 
    {
        return $this->status;
    }
    
    public function setScoreThuis($scorethuis) 
    {
        if (empty ($scorethuis))
        {
            return false;
        } else {
            $this->scoreThuis = $scoreThuis;
            return true;
        }
    }
    
    public function getScoreThuis() 
    {
        return $this->scoreThuis;
    }
    
    public function setScoreBezoekers($scorebezoekers) 
    {
        if (empty ($scoreBezoekers))
        {
            return false;
        } else {
            $this->scoreBezoekers = $scorebezoekers;
            return true;
        }
    }
    
    public function getScoreBezoekers() 
    {
        return $this->scoreBezoekers;
    }
}   
