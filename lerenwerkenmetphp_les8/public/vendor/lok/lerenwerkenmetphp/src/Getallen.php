<?php
namespace Lok\LerenWerkenMetPHP;

class Getallen
{
    public function declareNumbers()
    {
        $integer = 2;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        echo $integer * $integer;
        echo $integer / $integer;
        echo $integer - $integer;
        echo $integer + $integer;
        echo pow($integer, $integer);
    }
    
    public function boolean()
    {
        
        $b = false;
        var_dump($b);
        
        // $b = TRUE
        $b = true;
        var_dump($b);
        // Output: boolean true
        echo $b;
        // Output: 1;
    }
}
?>